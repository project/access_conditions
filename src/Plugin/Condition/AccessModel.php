<?php

namespace Drupal\access_conditions\Plugin\Condition;

use Drupal\access_conditions\AccessChecker;
use Drupal\access_conditions\Entity\AccessModel as AccessModelEntity;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'Access model' condition.
 *
 * @Condition(
 *   id = "access_conditions_access_model",
 *   label = @Translation("Access model"),
 * )
 */
class AccessModel extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  use RefinableCacheableDependencyTrait;

  /**
   * The access model service.
   *
   * @var \Drupal\access_conditions\AccessChecker
   */
  protected $accessChecker;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\access_conditions\Plugin\Condition\AccessModel $instance */
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setAccessChecker($container->get('access_conditions.access_checker'));

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'access_models' => [],
      'operator' => 'and',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $access_models = AccessModelEntity::loadMultiple();
    $access_models_options = array_map(function ($access_model) {
      return $access_model->label();
    }, $access_models);

    $form['access_models'] = [
      '#type' => 'select',
      '#title' => $this->t('Access models'),
      '#options' => $access_models_options,
      '#multiple' => TRUE,
      '#size' => 15,
      '#default_value' => $this->configuration['access_models'] ?? [],
    ];
    $form['operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Operator'),
      '#title_display' => 'invisible',
      '#options' => [
        'and' => $this->t('All access models must pass'),
        'or' => $this->t('Only one access models must pass'),
      ],
      '#default_value' => $this->configuration['operator'] ?? 'and',
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['access_models'] = array_filter($form_state->getValue('access_models'));
    $this->configuration['operator'] = $form_state->getValue('operator');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $t_args = [
      '@condition' => $this->configuration['operator'] == 'and' ? $this->t('All') : $this->t('Only'),
      '@access_models' => implode(',', $this->configuration['access_models']),
    ];
    if (!empty($this->configuration['negate'])) {
      return $this->configuration['operator'] == 'and' ?
        $this->t('All access models must pass: @access_models.', $t_args) :
        $this->t('Only one access model must pass: @access_models.', $t_args);
    }
    else {
      return $this->configuration['operator'] == 'and' ?
        $this->t('All access models must not pass: @access_models.', $t_args) :
        $this->t('Only one access model must not pass: @access_models.', $t_args);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    /** @var \Drupal\access_conditions\Entity\AccessModelInterface[] $access_models */
    $access_models = AccessModelEntity::loadMultiple($this->configuration['access_models']);

    $result = [];
    foreach ($access_models as $access_model) {
      $result[$access_model->id()] = $this->accessChecker->checkAccess($access_model);
      $this->addCacheableDependency($this->accessChecker);
    }

    $operator = $this->configuration['operator'];
    $result = array_reduce(
      $result,
      function ($a, $b) use ($operator) {
        if ($operator == 'or') {
          return ($a === NULL) ? $b : $a || $b;
        }

        return ($a === NULL) ? $b : $a && $b;
      }
    );

    return $result;
  }

  /**
   * Sets the access model service.
   *
   * @param \Drupal\access_conditions\AccessChecker $access_check
   *   The access model service.
   *
   * @return $this
   */
  protected function setAccessChecker(AccessChecker $access_check) {
    $this->accessChecker = $access_check;

    return $this;
  }

}
